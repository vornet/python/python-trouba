from .core import Foo
from ._meta import VERSION as __version__

__all__ = [
    'Foo',
]
